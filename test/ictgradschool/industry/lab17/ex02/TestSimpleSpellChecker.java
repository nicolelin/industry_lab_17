package ictgradschool.industry.lab17.ex02;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * TODO Implement this class.
 */
public class TestSimpleSpellChecker {

    @Test
    public void testFoundationsOfMathematics() {
        assertEquals(2, 1 + 1);
    }

    SimpleSpellChecker spellChecker;
    Dictionary dictionary = new Dictionary();

    @Before
    public void setupSpellChecker() {
        try {
            spellChecker = new SimpleSpellChecker(dictionary, "the quick brown fox jumps over the lazy dog the quick fox jumps nightly above wizard 1234567890 asdf 1234igv34q2ra");
        } catch (InvalidDataFormatException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetMisspelledWords() {
        List<String> testWordsList = new ArrayList<>(Arrays.asList("1234567890", "asdf", "1234igv34q2ra"));
        Collections.sort(testWordsList);
        List<String> misspelledWordsList = spellChecker.getMisspelledWords();
        Collections.sort(misspelledWordsList);
        assertEquals(testWordsList, misspelledWordsList);
    }

    @Test
    public void testGetUniqueWords() {
        List<String> testWordsList = new ArrayList<>(Arrays.asList("over", "quick", "1234igv34q2ra", "brown", "fox", "the", "nightly", "1234567890", "above", "asdf", "wizard", "lazy", "jumps", "dog"));
        Collections.sort(testWordsList);
        List<String> uniqueWordsList = spellChecker.getUniqueWords();
        Collections.sort(uniqueWordsList);
        assertEquals(testWordsList, uniqueWordsList);
    }

    @Test
    public void testGetFrequencyOfWord() throws InvalidDataFormatException {
        try {
            int[] result = {3, 2, 0, 1, 1, 0};
            String[] testInput = {"the", "fox", "hello", "brown", "1234567890", "wizards"};
            for (int i = 0; i < testInput.length; i++) {
                int frequency = spellChecker.getFrequencyOfWord(testInput[i]);
                assertEquals(testInput[i] + result[i], testInput[i] + frequency);
            }
        } catch (InvalidDataFormatException e) {
            e.printStackTrace();
        }
    }
}
package ictgradschool.industry.lab17.ex02;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * TODO Implement this class.
 */
public class TestDictionary {

    @Test
    public void testTrueIsTrue() {
        assertEquals(true, true);
    }

    private Dictionary dictionary;

    @Before
    public void setupDictionary() {
        dictionary = new Dictionary();
    }

    @Test
    public void testIsSpellCorrect() {
        assertTrue(dictionary.isSpellingCorrect("right"));
        assertTrue(dictionary.isSpellingCorrect("wrong"));
        assertTrue(dictionary.isSpellingCorrect("book"));
        assertFalse(dictionary.isSpellingCorrect("asdfasdf"));
        assertFalse(dictionary.isSpellingCorrect("123456789"));
        assertFalse(dictionary.isSpellingCorrect("][;'/l.,mn"));
    }

}
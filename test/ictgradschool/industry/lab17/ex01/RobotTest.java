package ictgradschool.industry.lab17.ex01;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/* TODO
 Exercise One: Completing the Robot Tests
        This exercise involves writing simple unit tests for the Robot program discussed in the lecture. You need to complete the RobotTest class to test the following methods of the Robot class:

public void turn()
         Write unit tests to check that the robot is facing the right direction after each turn. You also need to make sure that the robot hasn’t moved, but it has changed its direction by 90 degrees to the right.

public void move() throws IllegalMoveException
         Write unit tests to test for a valid move in each direction the robot is facing.
         Write unit tests to test for an invalid move (i.e. moving outside of the grid) in each direction the robot is facing.

        Other public methods in the Robot class
 Write unit tests for any other public methods in the Robot class.

        Hints
        There will be four test cases for the method turn and eight test cases for the method move.
        For each method you’re testing, do some brainstorming / write down in plain English what you want to test. This will help you plan your unit tests. */

public class RobotTest {

    private Robot myRobot;

    @Before
    public void setUp() {
        myRobot = new Robot();
    }

    @Test
    public void testRobotConstruction() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void testIllegalMoveNorth() {
        boolean atTop = false;

        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++) {
                myRobot.move();
            }

            // Check that robot has reached the top.
            atTop = myRobot.currentState().row == 1;
            assertTrue(atTop);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();

        } catch (IllegalMoveException e) {
            // Ensure the move didn't change the robot state
            assertEquals(1, myRobot.currentState().row);
        }
    }

    // My Test Units

    @Test
    public void testTurnNorthToEast() {
        myRobot.turn();
        assertEquals(Robot.Direction.East, myRobot.currentState().direction);
        assertEquals(myRobot.row(), 10);
        assertEquals(myRobot.column(), 1);
    }

    @Test
    public void testTurnEastToSouth() {
        while (myRobot.getDirection() != Robot.Direction.South) {
            myRobot.turn();
        }
        assertEquals(Robot.Direction.South, myRobot.currentState().direction);
        assertEquals(myRobot.row(), 10);
        assertEquals(myRobot.column(), 1);
    }

    @Test
    public void testTurnSouthToWest() {
        while (myRobot.getDirection() != Robot.Direction.West) {
            myRobot.turn();
        }
        assertEquals(Robot.Direction.West, myRobot.currentState().direction);
        assertEquals(myRobot.row(), 10);
        assertEquals(myRobot.column(), 1);
    }

    @Test
    public void testTurnWestToNorth() {
        while (myRobot.getDirection() != Robot.Direction.North) {
            myRobot.turn();
        }
        assertEquals(Robot.Direction.North, myRobot.currentState().direction);
        assertEquals(myRobot.row(), 10);
        assertEquals(myRobot.column(), 1);
    }

    @Test
    public void testMoveNorth() {
        if (myRobot.currentState().direction == Robot.Direction.North) {

            int oldRow = myRobot.currentState().row;
            int oldColumn = myRobot.currentState().column;

            try {
                myRobot.move();
            } catch (IllegalMoveException e) {
                assertTrue(true);
            }

            assertEquals(Robot.Direction.North, myRobot.currentState().direction);

            assertEquals(myRobot.row(), oldRow - 1);
            assertEquals(myRobot.column(), oldColumn);
        }
    }

    @Test
    public void testMoveEast() {

        // Keep turning robot until it face east
        while (myRobot.getDirection() != Robot.Direction.East) {
            myRobot.turn();
        }

        // Try to move the robot to the east
        if (myRobot.currentState().direction == Robot.Direction.East) {
            try {
                while (myRobot.column() < Robot.GRID_SIZE) {
                    int oldRow = myRobot.currentState().row;
                    int oldColumn = myRobot.currentState().column;
                    myRobot.move();
                    assertEquals(myRobot.row(), oldRow);
                    assertEquals(myRobot.column(), oldColumn + 1);
                }
            } catch (IllegalMoveException e) {
                fail();
            }
            assertEquals(Robot.Direction.East, myRobot.currentState().direction);
        }
    }

    @Test
    public void testMoveSouth() {

        // Make robot face north so we can move it to a position that will not cause illegal move exception
        while (myRobot.getDirection() != Robot.Direction.North) {
            myRobot.turn();
        }

        // Move the robot to the very top of the grid
        try {
            while (myRobot.row() < Robot.GRID_SIZE) {
                myRobot.move();
            }
        } catch (IllegalMoveException e) {
            fail();
        }

        // Make the robot face south so we can test moving south
        while (myRobot.getDirection() != Robot.Direction.South) {
            myRobot.turn();
        }

        if (myRobot.currentState().direction == Robot.Direction.South) {
            try {
                while (myRobot.row() < Robot.GRID_SIZE) {
                    int oldRow = myRobot.currentState().row;
                    int oldColumn = myRobot.currentState().column;
                    myRobot.move();
                    assertEquals(myRobot.row(), oldRow + 1);
                    assertEquals(myRobot.column(), oldColumn);
                }
            } catch (IllegalMoveException e) {
                fail();
            }
            assertEquals(Robot.Direction.South, myRobot.currentState().direction);
        }
    }

    @Test
    public void testMoveWest() {

        while (myRobot.getDirection() != Robot.Direction.West) {
            myRobot.turn();
        }

        if (myRobot.currentState().direction == Robot.Direction.West) {

            int oldRow = myRobot.currentState().row;
            int oldColumn = myRobot.currentState().column;

            try {
                myRobot.move();
            } catch (IllegalMoveException e) {
                assertTrue(true);
            }

            assertEquals(Robot.Direction.West, myRobot.currentState().direction);
            assertEquals(myRobot.row(), oldRow);
            assertEquals(myRobot.column(), oldColumn);
        }
    }

    @Test
    public void testIllegalMoveEast() {
        boolean atRight = false;

        try {
            // Turn the robot to face east.
            myRobot.turn();

            // Move the robot to the rightmost row.
            while (myRobot.column() < Robot.GRID_SIZE) {
                myRobot.move();
            }

            // Check that robot has reached the right.
            atRight = myRobot.currentState().column == 10;
            assertTrue(atRight);

        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move East.
            myRobot.move();
            fail();

        } catch (IllegalMoveException e) {
            // Ensure the move didn't change the robot state
            assertEquals(10, myRobot.currentState().column);
        }
    }

    @Test
    public void testIllegalMoveSouth() {
        boolean atBottom = false;

        // Turn robot to face south
        while (myRobot.getDirection() != Robot.Direction.South) {
            myRobot.turn();
        }

        // Move robot until reach bottom row
        try {
            while (myRobot.row() < Robot.GRID_SIZE) {
                myRobot.move();
            }

            // Check robot has reached bottom row
            atBottom = myRobot.currentState().row == 10;
            assertTrue(atBottom);

        } catch (IllegalMoveException e) {
            fail();
        }

        // Keep attempting to move robot south
        try {
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Check move did not change robot state
            assertEquals(10, myRobot.currentState().row);
        }
    }

    @Test
    public void testIllegalMoveWest() {

        // Turn robot to face west
        while (myRobot.getDirection() != Robot.Direction.West) {
            myRobot.turn();
        }

        // Keep attempting to move robot west
        try {
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {

            // Check move did not change robot state
            assertEquals(1, myRobot.currentState().column);
        }
    }

}

package ictgradschool.industry.lab17.ex04;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by ylin183 on 1/06/2017.
 */
public class TestShapeCalculator {
    private ShapeCalculator shapeCalculator;

    @Before
    public void setUp() {
        shapeCalculator = new ShapeCalculator();
    }

    @Test
    public void testAreaCalcRec() {
        assertEquals(379.25, shapeCalculator.getAreaOfRectangle(20.5, 18.5), 0.01);
    }

    @Test
    public void testAreaCalcCircle() {
        assertEquals(1256.6370614359173, shapeCalculator.getAreaOfCircle(20.0), 0.01);
    }

    @Test
    public void testRoundingNumbers() {
        assertEquals(379, shapeCalculator.getRoundedNumber(379.25));
        assertEquals(1257, shapeCalculator.getRoundedNumber(1256.6370614359173));
    }

}

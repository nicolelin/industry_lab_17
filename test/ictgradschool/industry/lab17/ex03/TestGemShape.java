package ictgradschool.industry.lab17.ex03;

/**
 * Created by ylin183 on 31/05/2017.
 */

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

// NOTE - if code is shared between classes, and has been tested, don't test again. Only test what is different.
// Just need to test drawing the gem shape. => 2 - big and small

public class TestGemShape {
    private MockPainter painter;

    @Before
    public void setupMockPainter() {
        painter = new MockPainter();
    }

    @Test
    public void testDefaultConstructor() {
        GemShape shape = new GemShape();

        assertEquals(0, shape.getX());
        assertEquals(0, shape.getY());
        assertEquals(5, shape.getDeltaX());
        assertEquals(5, shape.getDeltaY());
        assertEquals(25, shape.getWidth());
        assertEquals(35, shape.getHeight());

        shape.paint(painter);
        assertEquals("(polygon xpoints: [0, 12, 25, 12], ypoints: [17, 0, 17, 35])", painter.toString());
    }

    @Test // Test shapes with width < 40
    public void testConstructorWithAllValuesWithWidthLessThan40() {
        GemShape shape = new GemShape(1, 2, 3, 4, 5, 6);

        assertEquals(1, shape.getX());
        assertEquals(2, shape.getY());
        assertEquals(3, shape.getDeltaX());
        assertEquals(4, shape.getDeltaY());
        assertEquals(5, shape.getWidth());
        assertEquals(6, shape.getHeight());

        shape.paint(painter);
        assertEquals("(polygon xpoints: [1, 3, 6, 3], ypoints: [5, 2, 5, 8])", painter.toString());
    }

    @Test // Test shapes with width > 40
    public void testConstructorWithAllValuesWithWidthMoreThan40() {
        GemShape shape = new GemShape(10, 20, 30, 40, 50, 60);

        assertEquals(10, shape.getX());
        assertEquals(20, shape.getY());
        assertEquals(30, shape.getDeltaX());
        assertEquals(40, shape.getDeltaY());
        assertEquals(50, shape.getWidth());
        assertEquals(60, shape.getHeight());

        shape.paint(painter);
        assertEquals("(polygon xpoints: [10, 30, 40, 60, 40, 30, 0, 0], ypoints: [50, 20, 20, 50, 80, 80, 0, 0])", painter.toString());
    }

}

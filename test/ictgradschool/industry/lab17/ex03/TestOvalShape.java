package ictgradschool.industry.lab17.ex03;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
/**
 * Created by ylin183 on 31/05/2017.
 */

// NOTE - if code is shared between classes, and has been tested, don't test again. Only test what is different.
// Test if it draws an oval => 1 test

public class TestOvalShape {
    private MockPainter painter;

    @Before
    public void setupMockPainter() {
        painter = new MockPainter();
    }

    @Test
    public void testDefaultConstructor() {
        OvalShape shape = new OvalShape();

        assertEquals(0, shape.getX());
        assertEquals(0, shape.getY());
        assertEquals(5, shape.getDeltaX());
        assertEquals(5, shape.getDeltaY());
        assertEquals(25, shape.getWidth());
        assertEquals(35, shape.getHeight());

        shape.paint(painter);
        assertEquals("(oval 0,0,25,35)",painter.toString());
    }

}

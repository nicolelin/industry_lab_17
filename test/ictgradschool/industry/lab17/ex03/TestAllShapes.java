package ictgradschool.industry.lab17.ex03;

/**
 * Created by ylin183 on 31/05/2017.
 */

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        TestNestingShape.class,
        TestRectangleShape.class,
        TestOvalShape.class,
        TestGemShape.class,
        TestDynamicRectangleShape.class
})

public class TestAllShapes {
}
package ictgradschool.industry.lab17.ex04;

/**
 * Created by ylin183 on 31/05/2017.
 */

import ictgradschool.Keyboard;

public class ShapeCalculator {

    private double width;
    private double length;
    private double radius;

    public void start() {

        System.out.println("Welcome to the Shape Area Calculator!");

        while (true) {
            try {
                userInput();
                break;
            } catch (NullPointerException e) {
                System.out.println("No input detected. Please enter a number.");
            } catch (NumberFormatException e) {
                System.out.println("Incorrect format. Please enter a valid number.");
            }
        }

        calcArea();

    }

    public void userInput() {
        System.out.print("Enter the width of the rectangle: ");
        width = Double.parseDouble(Keyboard.readInput());
        System.out.print("Enter the height of the rectangle: ");
        length = Double.parseDouble(Keyboard.readInput());
        System.out.print("Enter the radius of the circle: ");
        radius = Double.parseDouble(Keyboard.readInput());
    }

    public int getRoundedNumber(double number) {
        return (int) Math.round(number);
    }

    public double getAreaOfRectangle(double width, double length) {
        Double area = width * length;
        System.out.println("Rectangle area in Double: " + area);
        return area;
    }

    public double getAreaOfCircle(double radius) {
        Double area = Math.PI * Math.pow(radius, 2);
        System.out.println("Circle area in Double: " + area);
        return area;
    }

    public void calcArea() {
        int areaRec = getRoundedNumber(getAreaOfRectangle(width,length));
        int areaCircle = getRoundedNumber(getAreaOfCircle(radius));

        System.out.println("The radius of the circle is: " + radius);

        System.out.println("Calculating...");

        System.out.println("The area of the rectangle is: " + areaRec);
        System.out.println("The area of the circle is: " + areaCircle);

        System.out.println("The smaller area is: " + Math.min(areaRec,areaCircle));
    }

    public static void main(String[] args) {
        new ShapeCalculator().start();
    }

}
